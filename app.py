# -*- coding: utf-8 -*-

from flask import Flask, request, render_template, make_response, redirect, url_for, g
from modules import moduleAuth

from sqlalchemy import create_engine
from werkzeug.local import LocalProxy

import json
import sys
import os
import uuid


app = Flask(__name__)
app._static_folder = 'static'

# http://docs.sqlalchemy.org/en/rel_0_9/core/engines.html
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:xxx@localhost/site'


def get_db():
  
    """ db connection """
  
    db = getattr(g, '_database', None)
    if db is None:
        e = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
        db = g._database = e.connect()
    return db


"""
  make global db variable
  http://flask.pocoo.org/docs/0.10/appcontext/
"""
db = LocalProxy(get_db)

# pass env to moduleAuth
moduleAuth.db = db

# and request
moduleAuth.request = request


@app.teardown_appcontext
def teardown_db(exception):
  
    """ close db connection"""
  
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route("/logout")
def logout():
    
    """ close session """
    
    resp = make_response(redirect(url_for('index')))
    resp.set_cookie('session_token', '', expires=0)
    return resp


@app.route("/")
def index():

    """ index page """
    
    auth = moduleAuth.is_auth(request)
    if auth != False:
        resp = make_response(render_template("index.html", user_id = auth))
        return resp
    else:
        resp = make_response(render_template("index.html"))
        return resp


@app.route("/login", methods=['POST', 'GET'])
def login():
    
    """
    // compatible with 
    // https://github.com/drostie/sha3-js/blob/master/keccak.js
    s = hashlib.new("sha3_256")
    s = hashlib.sha3_256()
    s.update('qazwsx'.encode('utf-16-le', 'ignore'))
    a = s.hexdigest()
    """
    
    # already authenticated ?
    auth = moduleAuth.is_auth(request)
    if auth is not False:
        return redirect(url_for('index'))
    
    # request for auth
    errors = {}
    if request.method == 'POST':
        fields = moduleAuth.validateLoginFields(request.form)
        # first validate fields
        if fields is True:
            auth = moduleAuth.validateAuth(request.form)
            if auth is not False:
                # start user session
                for row in auth:
                    login = row['login']
                session_token = moduleAuth.start_session(row['id'])
                response = make_response(redirect(url_for('index')))
                response.set_cookie('session_token', session_token)
                return response
            else:
                errors = 'wrong login or password'
        else:
            errors=fields

    csrf_token = moduleAuth.getCSRF()
    response = make_response(render_template("login.html", csrf = csrf_token, error=errors))
    response.set_cookie('csrf-token', value=csrf_token)
    return response


@app.route("/signup")
def signup():
    
    token = moduleAuth.getCSRF()
    response = make_response(render_template("signup.html", csrf = token))
    response.set_cookie('csrf-token', value=token)
    return response


@app.route("/api/login", methods=['POST'])
def api_login():
    
    """ Login check """
    
    if request.method == 'POST':

        # check csrf first
        token = moduleAuth.getCSRF()
        csrf = checkCSRF(token)

        if csrf is not True:
            resp = make_response(render_template("login.html", 
                error = 'CSRF error', csrf = token))
            return resp
        
        # encode password
        s = hashlib.new("sha3_256")
        s = hashlib.sha3_256()
        s.update(request.form['pwd'].encode('utf-16-le', 'ignore'))
        a = s.hexdigest()

        # validate login parameters
        pwd = a
        lgn = request.form['lgn']
        auth.validateLogin({"password": pwd, "login": lgn})
        
        table = 'email' if is_email(lgn) else 'login'
        
        query = """ SELECT t1.* 
                FROM users t1 
                WHERE t1.password = '%(pwd)s'
                AND t1.%(tbl)s = '%(lgn)s'
                """ % {"pwd" : pwd, "tbl": table, "lgn" : lgn}
        res = db.execute(query)
        
        if int(res.rowcount) > 0:
            for row in res:
                login = row['login']
            session_token = start_session(row['id'])
            response = make_response(redirect(url_for('index')))
            response.set_cookie('session_token', session_token)
            return response
        else:
            resp = make_response(render_template("login.html", 
                error = 'error login or password', csrf=token))
            return resp
    else:
        return json.dumps({'code': 'unsupported'})


@app.route("/api/signup", methods=['POST'])
def api_signup():
    
    """ Signup """
    
    if request.method == 'POST':
        token = moduleAuth.getCSRF()

        # check csrf first
        csrf = request.cookies.get('csrf-token')
        if csrf != request.json['csrf-token']:
            return json.dumps({'code': 'csrf'})
        
        pwd = request.json['pwd']
        lgn = request.json['lgn']
        email = request.json['email']
        
        # double check
        if len(lgn) < 6:
            return json.dumps({'code': 1})
            
        if is_email(email) == False:
            return json.dumps({'code': 'incorrect_email'})
            
        query = """
                SELECT id FROM users WHERE login = '%(lgn)s'
                """ % {"lgn": lgn}
        res = db.execute(query)
        if int(res.rowcount) > 0:
            return json.dumps({'code': 'login_duplicate'})
            
        query = """
                SELECT id FROM users WHERE email = '%(email)s'
                """ % {'email': email}
        res = db.execute(query)
        if int(res.rowcount) > 0:
            return json.dumps({'code': 'email_duplicate'})
        
        query = """
                INSERT INTO users (id, login, password, email)
                VALUES (nextval('users_index'), '%(lgn)s', '%(pwd)s', '%(email)s')
                """ % {'lgn': lgn, 'email': email, 'pwd': pwd}
        res = db.execute(query)
        
    else:
        return json.dumps({'code': 'unsupported'})


if __name__ == "__main__":
  app.run(debug=True)



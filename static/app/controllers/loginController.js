
/**
 * login controller
 */

app = angular.module('auth', []);

app.constant('apiConfig', {
    login_api: '/api/login'
});

app.factory('langService', function($http) {
    var path = '/static/i18n/ru.json';
    return {
        getLang: function() {
            return $http.get(path);
        }
    }
});

app.controller('loginController', ['$scope', 'langService', '$http', '$sce',
function($scope, langService, $http, $sce) {
    
    langService.getLang().then(function(response) {
        $scope.i18n = response.data
        $scope.rules = $sce.trustAsHtml([
            $scope.i18n['ERROR']['LOGIN_REQUIRED'],
            $scope.i18n['ERROR']['LOGIN_FOREIGN'],
            $scope.i18n['ERROR']['PWD_REQUIRED']
        ].join("<br>"))
    });
    
    $scope.login = '';
    $scope.password = '';
    $scope.login_api = '/api/login';
    $scope.signup_api = '/api/signup';
    
    // redirect to signup
    $scope.toSignUp = function() {
        window.location = '/signup';
    }
    
    // redirect to login
    $scope.toLogIn = function() {
        window.location = '/login';
    }
    
    // submit handler
    $scope.submit = function() {
        
        var err;
        document.getElementById('form-alert').style.display = 'none';
        
        var login = document.getElementById('formUser').value,
            pwd = document.getElementById('formPwd').value,
            token = document.getElementById('csrf_token').innerHTML;
        
        if (this.isEmail(login)) {
            this.validate_password(pwd);
            return;
        }
        
        if (login.length < 6) {
            err = $scope.i18n['ERROR']['LOGIN_REQUIRED'];
            this.show_error(err);
            return;
        }
        
        var rforeign = /[^\u0000-\u007f]/;
        if (rforeign.test(login)) {
            err = $scope.i18n['ERROR']['LOGIN_FOREIGN'];
            this.show_error(err)
            return;
        }
        
        var email = document.getElementById('formEmail');
        if (email) {
            if (!this.isEmail(email.value)) {
                err = $scope.i18n['ERROR']['EMAIL_WRONG'];
                this.show_error(err);
                return;
            }
        }
        
        if (this.validate_password(pwd)) {
            var pwd_hash = keccak(pwd)
            $scope.data = {
                'pwd': pwd_hash, 
                'lgn': encodeURIComponent($scope.login),
                'csrf_token': token
            }
            if (email) $scope.data['email'] = email.value;
            
            $http({
                method: 'POST', 
                data: $scope.data, 
                url: email ? $scope.signup_api :$scope.login_api, 
                cache: false
            }).
            success(function(data, status) {
                try {
                    var parsed = angular.fromJson(data)
                    if (parsed['code'] == 'login_duplicate') {
                        err = $scope.i18n['ERROR']['LOGIN_DUPLICATE'];
                    }
                    if (parsed['code'] == 'email_duplicate') {
                        err = $scope.i18n['ERROR']['EMAIL_DUPLICATE'];
                    }
                    if (parsed['code'] == 1) {
                        err = $scope.i18n['ERROR']['LOGIN_WRONG'];
                    }
                    if (err) {
                        $scope.show_error(err);
                        return;
                    }
                    window.location = '/';
                } catch(err) {
                    alert(err)
                }
            }).
            error(function(data, status) {
                console.log('ERROR: ' + data)
            })
        } else { return false; }
    }
  
  // login onChange
  $scope.login_change = function() {
    var el = document.getElementById('formUser').value;
    if (this.validate_foreign(el)) {
      var err = $scope.i18n['ERROR']['LOGIN_FOREIGN'];
      this.show_error(err)
      return;
    } else { document.getElementById('form-alert').style.display = 'none'; }
  }
  
  // looking for invalid symbols
  $scope.validate_foreign = function(sym) {
    var rforeign = /[^\u0000-\u007f]/;
    if (rforeign.test(sym)) {
      return true;
    } else { return false; }
  }
  
  // password - check for errors
  $scope.validate_password = function(pwd) {
    if (pwd.length < 8) {
      err = $scope.i18n['ERROR']['PWD_REQUIRED'];
      this.show_error(err)
      return false;
    } else { return true; }
  }
  
    // show login errors
    $scope.show_error = function(err) {
        if (err) {
            document.getElementById('form-alert').innerHTML = err;
            document.getElementById('form-alert').style.display = 'block';
        }
    }

  // email ?
  $scope.isEmail = function(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  
}]);

//angular.bootstrap(document, ['auth']);





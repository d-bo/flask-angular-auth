import re
import hashlib
import sha3
import time

from werkzeug.local import LocalProxy

from flask import g

# objects passed from app.py
db = {}
request = {}


def getCSRF():

    """ check or generate CSRF session token """
    
    csrf = request.cookies.get('csrf-token')
    token = csrf if csrf else sha3(time.time())
    return token


def checkCSRF(token):

    """ check session token """

    csrf = request.cookies.get('csrf-token')
    if csrf != token:
        return False
    else:
        return True


def is_email(email):
    
    """ validate email """
    
    email = str(email)
    if re.match("^[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$", email):
        return True
    else:
        return False


def sha3(string):
    
    """ keccak SHA3 """
    
    if string:
        string = str(string)
        s = hashlib.sha3_512()
        s.update(string.encode('utf-8'))
        a = s.hexdigest()
    return a


def validateLoginFields(form):

	""" checking auth data """

	email = is_email(form['lgn'])
	if email is not True:
		# login length
		if len(form['lgn']) < 5:
			return 'too short login. min 6 symbols'
	if len(form['pwd']) < 7:
		return 'too short password. min 7 symbols'
	return True


def validateAuth(form):

    # encode password using keccak
    s = hashlib.new("sha3_256")
    s = hashlib.sha3_256()
    s.update(form['pwd'].encode('utf-16-le', 'ignore'))

    # validate login parameters
    pwd = s.hexdigest()
    lgn = form['lgn']

    table = 'email' if is_email(lgn) else 'login'
    
    query = """ SELECT t1.* 
            FROM users t1 
            WHERE t1.password = '%(pwd)s'
            AND t1.%(tbl)s = '%(lgn)s'
            """ % {"pwd" : pwd, "tbl": table, "lgn" : lgn}
    res = db.execute(query)

    if int(res.rowcount) > 0:
        return res
    else:
        return False


def is_auth(request):
    
    """ Is user authenticated """

    session_token = request.cookies.get('session_token')
    query = """ SELECT * FROM sessions s
                INNER JOIN users u ON s.user_id = u.id
                INNER JOIN roles r ON u.role_id = r.id
                WHERE s.session_token = '%(session_token)s'
            """ % {"session_token": session_token}
    res = db.execute(query)
    if int(res.rowcount) > 0:
        for row in res:
            user_id = row['user_id']
        return row
    else:
        return False


def start_session(user_id):
    
    """ sesion startup """

    session_token = sha3(time.time())
    query = """ INSERT INTO sessions (id, user_id, session_token)
                VALUES (nextval('sessions_index'), '%(user_id)s', '%(session_token)s')
            """ % {"user_id": user_id, "session_token" : session_token}
    res = db.execute(query)
    
    return session_token
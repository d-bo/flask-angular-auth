# Playing with Flask + AngularJS

## DB
```
pip install SQLAlchemy
+ http://www.stickpeople.com/projects/python/win-psycopg/ (psycopg2)

table `users`:
CREATE SEQUENCE users_index START 1; (postgre)

table `sessions`:
CREATE SEQUENCE sessions_index START 1; (postgre)

table `roles`:
CREATE SEQUENCE roles_index START 1; (postgre)
```

## KECCAK
```
https://pypi.python.org/pypi/pysha3/
```